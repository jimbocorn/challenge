package com.challenge.validator;

import com.challenge.model.FileUpload;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.activation.MimetypesFileTypeMap;

public class FileUploadValidator implements Validator{

    @Override
    public boolean supports(Class clazz) {
        //just validate the FileUpload instances
        return FileUpload.class.isAssignableFrom(clazz);

    }

    @Override
    public void validate(Object target, Errors errors) {

        FileUpload file = (FileUpload)target;

        String mimetype= new MimetypesFileTypeMap().getContentType(file.getFile().getContentType());
        String type = mimetype.split("/")[0];
        if(file.getFile().getSize()==0) {
            errors.rejectValue("file", "required.fileUpload");
        }
        if (!type.equals("image")) {
            errors.rejectValue("file","required.imageType");
        }
    }

}