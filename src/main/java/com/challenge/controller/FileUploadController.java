package com.challenge.controller;

import com.challenge.model.FileUpload;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class FileUploadController extends SimpleFormController{

    private static final String LOCAL_IMAGES = "localImages";
    private static final String TOMCAT_HOME_PROPERTY = "catalina.home";
    private static final String TOMCAT_HOME_PATH = System.getProperty(TOMCAT_HOME_PROPERTY);
    private static final String LOCAL_IMAGES_PATH = TOMCAT_HOME_PATH + File.separator + LOCAL_IMAGES;

    private static final File LOCAL_IMAGES_DIR = new File(LOCAL_IMAGES_PATH);
    private static final String LOCAL_IMAGES_DIR_ABSOLUTE_PATH = LOCAL_IMAGES_DIR.getAbsolutePath() + File.separator;

    private static final String FAILED_UPLOAD_MESSAGE = "You failed to upload [%s] because the file because %s";


    public FileUploadController(){
        setCommandClass(FileUpload.class);
        setCommandName("fileUploadForm");
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
                                    HttpServletResponse response, Object command, BindException errors)
            throws Exception {

        FileUpload file = (FileUpload)command;

        MultipartFile multipartFile = file.getFile();
        Map<String, Object> model = new HashMap<String, Object>();
        String fileName="";
        String filetype="";

        if(multipartFile!=null) {
            createDirIfNeeded();
            fileName = multipartFile.getOriginalFilename();
            filetype = multipartFile.getContentType();
            model.put("fileName",createImage(fileName, multipartFile));
            model.put("fileType",filetype);
        }
        return new ModelAndView("FileUploadSuccess","model",model);

    }

    private void createDirIfNeeded() {
        if (!LOCAL_IMAGES_DIR.exists()) {
            LOCAL_IMAGES_DIR.mkdirs();
        }
    }

    private String createImage(String name, MultipartFile file) {
        try {
            File image = new File(LOCAL_IMAGES_DIR_ABSOLUTE_PATH + name);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(image));
            stream.write(file.getBytes());
            stream.close();

            return name;
        } catch (Exception e) {
            return String.format(FAILED_UPLOAD_MESSAGE, name, e.getMessage());
        }
    }

	@Override
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder)
		throws ServletException {

		// Convert multipart object to byte[]
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());

	}

}
